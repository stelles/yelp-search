"""
Main python program, this will include a command line interface for the yelp search API class included in src
"""
import sys
from Handler import *
from YelpEncoder import *




if __name__ == "__main__":

    print "Please get your OAuth credentials ready."
    print "They are located here: https://www.yelp.com/developers/manage_api_keys"

    consumer_key        = ''
    consumer_secret     = ''
    token               = ''
    token_secret        = ''


    while consumer_key == '':
        consumer_key = raw_input("Please enter your consumer_key: ")

    while consumer_secret == '':
        consumer_secret = raw_input("Please enter your consumer_secret: ")

    while token == '':
        token = raw_input("Please enter your token: ")

    while token_secret == '':
        token_secret = raw_input("Please enter your token_secret: ")


    print "Authorizing..."
    yelp = YelpApiHandler(consumer_key  = consumer_key,
                        consumer_secret = consumer_secret,
                        token           = token,
                        token_secret    = token_secret)

    # Test Search
    business_list = yelp.search(location='Seattle', term='thai')
    for bus in business_list:
        bus.pretty_print()

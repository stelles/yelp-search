"""
This file contains the datastructures for the returned API requests.
"""

# user: Toma
# added: April 21, 2015
# Notes will be added with prefix: #**#

from json import dumps, JSONEncoder


class YelpEncoder:
    #**# pretty but can just call __dict__ 
    def toDict(self):
        return self.__dict__

    #**# nice
    #**# http://stackoverflow.com/questions/706101/python-json-decoding-performance/5821091#5821091
    #**# Sub note: http://api.mongodb.org/python/current/api/bson/json_util.html (for dumping mongo documents)
    def jsonify(self):
        return dumps(self.toDict())

    #**# double nice
    def pretty_print(self):
        print dumps(self.toDict(), sort_keys=True,
                        indent=4, separators=(',', ': '))

class Business(YelpEncoder):
    """ This contains the basic data structure for a business found on yelp.  The data found here can be found by
        doing a Yelp business API request and contains a bit more than the simple Search data.
        Parameters:
            yelp_id      : string - Contains the string ID of the business
            rating       : float  - Contains the average rating
            review_count : int    - Number of reviews
            name         : string - Basic Name of the business
            categories   : list   - contains a list of categories
            review       : Review - contains the Review object
    """
    def __init__(self,
                yelp_id,
                rating,
                review_count,
                name,
                categories,
                location,
                review):
        self.id             = yelp_id
        self.rating         = rating
        self.review_count   = review_count
        self.name           = name
        self.categories     = categories
        self.location       = location
        self.review         = review

    #**# https://docs.python.org/2/library/copy.html#copy.deepcopy
    def toDict(self):
        """Simple function to override the toDict and allow for a complete dicinoary of the business object."""
        d = self.__dict__.copy()

        #**# helpful when iterating over a large dict 
        #**# better to try the key and catch the exception http://bit.ly/1Qh58Ka
        #**# https://docs.python.org/2/library/collections.html#collections.defaultdict
        if 'review' in d:
            d['review'] = d['review'].__dict__
        return d

class Review(YelpEncoder):
    """ Simple data class to contain the important review information easily expanded."""
    def __init__(self,
                rating,
                excerpt):
        self.rating     = rating
        self.excerpt    = excerpt








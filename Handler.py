"""
Yelp Data Integration Handler program
"""
#**# user: Toma
#**# added: April 21, 2015
#**# Notes will be added with prefix: #**#

from rauth import *

from YelpEncoder import *

class YelpApiHandler:
    """ This class will handle the interaction with the yelp API as well as 
    allow for easy data handling from the responses.  The functions here will
    act as a wrapper to both OAuth as well as the Data handling."""

    BASE_URL = 'http://api.yelp.com'
    SEARCH_URL = '/v2/search/'
    BUSINESS_URL = '/v2/business/'

    
    def __init__(self, consumer_key, consumer_secret, token, token_secret):
        """ Construction of this class will require the 4 necessities to the Oauth
        system, these can be found at 'https://www.yelp.com/developers/manage_api_keys'
        After construction this class, use the functions below to access the yelp API"""

        self.Connection = OAuth1Session(consumer_key, consumer_secret, token,token_secret)
        self.__oauthTest()


    def __oauthTest(self):
        """ Simple test to make sure the keys are working correctly. """
        resp = self._getRequest(self.SEARCH_URL, {'location':'Seattle', "term":"pizza"})
        if 'error' in resp:
            print "ERROR:"
            print resp['error']['text']
            print resp['error']['id']
            print resp['error']['field']
            exit()

    def _objectify(self, json):
        """ This funciton was built to pass a single JSON data structure to convert into a Business object. """
        
        if 'reviews' in json:
            yelp_review = Review(rating  = json['reviews']['rating'], 
                                excerpt  = json['reviews']['exceprt'])
        else:
            yelp_review = Review(rating  = json['rating'], 
                                excerpt  = json['snippet_text'])
      
        return Business(yelp_id         = json['id'],
                        rating          = json['rating'],
                        review_count    = json['review_count'],
                        name            = json['name'],
                        categories      = json['categories'],
                        location        = json['location']['coordinate'],
                        review          = yelp_review)



    def _getRequest(self, query_type, params):
        """ This is the basic requst function from the yelp APU"""
        response = self.Connection.get(self.BASE_URL + query_type, params=params)
        return response.json()


    def search(self, location, term, **params):
        """ The search function allows for extra parameters other than location and term.

            EX: yelp.search(location='Seattle', term='bananas', limit=10)

            This would limit the search to the top 10 businesses. 
            Full parameter list found here: https://www.yelp.com/developers/documentation/v2/search_api
            """

        params['location']  = location
        params['term']      = term

        # Number of max results per request
        offset = 0
        response = self._getRequest(self.SEARCH_URL, params=params)
        
        business_list = []
        while offset <= response['total']:
            params['offset'] = offset
            response = self._getRequest(self.SEARCH_URL, params=params)
            if 'businesses' in response:
                for bus in response['businesses']:
                    yield self._objectify(bus) # generator :) 
                    #**# business_list.append(self._objectify(bus)) 
                offset += 20
        #**# return business_list

        
        
    






